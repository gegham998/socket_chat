<?php

namespace App\Http\Controllers;

use App\Events\ChatMessage;
use App\Models\Comment;
use Illuminate\Support\Facades\Request;

class TasksController extends Controller
{
    public function comment(Request $request)
    {
        /**
         * Валидация. Добавляю сообщение в базу,
         * получаю модель Comment $comment с сообщением
         */

        event(new ChatMessage($comment)); // Это для примера. Отправка сообщения всем активным пользователям канала
        broadcast(new ChatMessage($comment))->toOthers(); // Отправляю сообщение всем, кроме текущего пользователя
    }
}